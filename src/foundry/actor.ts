export interface IActor {
    _id: string;
    name: string;
    img: string;
    type: 'hazard'|'npc';
    data: object;
    items: Item[];
    folder: string;
}

export class Actor<T> {
    _id: string;
    name: string = '';
    img: string = 'icons/svg/mystery-man.svg';
    type: 'hazard'|'npc';
    data: T;
    items: Item[] = [];
    folder: string;
}




export class DamageInfo {
    damage: string;
    damageType: string;
}

export class Item {
    _id: string;
    flags: any = {};
    name: string;
    img: string = "icons/svg/mystery-man.svg";
}

export class ActionItem extends Item {
    type: "action" = "action";
    data: ActionItemDetails = new ActionItemDetails();
}

export class MeleeItem extends Item {
    type: "melee" = "melee";
    data: MeleeItemDetails = new MeleeItemDetails();
}

export class LoreItem extends Item {
    type: 'lore' = 'lore';
    data: LoreItemDetails = new LoreItemDetails();
}

export class EquipmentItem extends Item {
    type: 'equipment' = 'equipment';
    data: EquipmentItemDetails = new EquipmentItemDetails();
}

export class ActionItemDetails {
    description: {
        value: string;
    } = { value: "" };
    source: {
        value: string;
    } = { value: "" };
    traits: {
        value: string[],
        custom: string;
    } = { value: [], custom: "" };
    actionType: {
        value: "action"|"reaction"|"free"|"passive";
    } = { value: "reaction" };
    actions: { value: string; } = { value: "" }
}

export class MeleeItemDetails {
    description: {
        value: string;
    } = { value: "" };
    source: {
        value: string;
    } = { value: "" };
    traits: {
        value: string[],
        custom: string;
    } = { value: [], custom: "" };
    bonus: { value: number; } = { value: 0 };
    damageRolls: DamageInfo[] = [];
    attackEffects: {
        value: string[];
        custom: string;
    } = { value: [], custom: "" };
    weaponType: {
        value: "melee"|"ranged";
    } = { value: "melee" };
}

export class LoreItemDetails {
    description: { value: string; }
    mod: { value: number }

    constructor() {
        Object.assign(this, {
            description: { value: '' },
            mod: { value: 0 }            
        } as LoreItemDetails);
    }
}

export class EquipmentItemDetails {
    description: { value: string; }
    quantity: { value: number; }

    constructor() {
        Object.assign(this, {
            description: { value: '' },
            quantity: { value: 1 }
        } as EquipmentItemDetails);
    }
}

export class SaveDetails {
    value: number = 0;
    saveDetail: string = "";
}

export class AbilityDetails {
    value: number;
    mod: number;
}