import * as Actor from './actor'
import { HazardDetails } from './hazard';

export class NPCActor extends Actor.Actor<NPCData> {
    constructor() {
        super();
        this.type = 'npc';
        this.data = new NPCData();
    }
}

export class NPCData {
    abilities: {
        str: Actor.AbilityDetails;
        dex: Actor.AbilityDetails;
        con: Actor.AbilityDetails;
        int: Actor.AbilityDetails;
        wis: Actor.AbilityDetails;
        cha: Actor.AbilityDetails;
    }
    saves: {
        fortitude: Actor.SaveDetails;
        reflex: Actor.SaveDetails;
        will: Actor.SaveDetails;
    }
    attributes: Attributes;
    details: Details;
    traits: NPCTraits;

    constructor() {
        Object.assign(this, {
            abilities: {
                str: { value: 10, mod: 0 },
                dex: { value: 10, mod: 0 },
                con: { value: 10, mod: 0 },
                int: { value: 10, mod: 0 },
                wis: { value: 10, mod: 0 },
                cha: { value: 10, mod: 0 },
            },
            saves: {
                fortitude: new Actor.SaveDetails(),
                reflex: new Actor.SaveDetails(),
                will: new Actor.SaveDetails()
            },
            attributes: {
                ac: {
                    value: 0,
                    details: ''
                },
                allSaves: {
                    value: ''
                },
                shield: {
                    value: 0,
                    max: 0,
                    brokenThreshold: 0,
                    hardness: 0,
                    ac: 2
                },
                hp: {
                    value: 0,
                    max: 0,
                    temp: 0,
                    details: ''
                },
                perception: {
                    value: 0
                },
                speed: {
                    value: '',
                    special: '',
                    otherSpeeds: []
                }
            },
            details: {
                alignment: { value: '' },
                level: { value: 0 },
                source: { value: '' },
            },
            traits: {
                size: { value: 'med' },
                senses: { value: '' },
                traits: {
                    value: [],
                    custom: ""
                },
                languages: {
                    value: [],
                    custom: ''
                },
                di: {
                    custom: "",
                    value: []
                },
                dr: [],
                dv: [],
                ci: [],
                rarity: { value: 'common' }
            },
        } as Partial<NPCData>);
    }
}

export class NPCDetails {
    source: { value: string };
    flavourText: string;
    nethysUrl: string;
    recallKnowledgeText: string;
    sidebarText: string;
    details: {
        isComplex: boolean;
        level: number;
        disable: string;
        description: string;
        reset: string;
        routine: string;
    };
    traits: {
        traits: {
            value: string[];
            custom: string;
        },
        di: { custom: string, value: string[] },
        dr: any[];
        dv: any[];
        ci: any[];
    };
}


class Attributes {
    ac: {
        value: number,
        details: string
    }
    allSaves: {
        value: string;
    }
    shield: {
        value: number;
        max: number;
        brokenThreshold: number;
        hardness: number;
        ac: number;
    }
    hp: {
        value: number;
        max: number;
        temp: number;
        details: string;
    }
    perception: {
        value: number;
    }
    speed: {
        value: string;
        special: string;
        otherSpeeds: object[]
    }
}

class Details {
    alignment: { value: string }
    level: { value: number }
    source: { value: string }
}

class NPCTraits {
    size: { value: string }
    senses: { value: string }
    languages: {
        value: string[],
        custom: string
    }
    rarity: { value: 'common' }

    traits: {
        value: string[];
        custom: string;
    }
    di: { custom: string, value: string[] }
    dr: any[];
    dv: any[];
    ci: any[];
}
 