import { ModuleDescription, ModuleData } from "./IAdventure";
import { ModuleScene } from "./ModuleScene";
import { PdfData, TextDetail } from "../parse";
import { PageLayoutEmitFunction, PageLayout } from "./PageLayouts";
import { Journal, MeleeItem, ActionItem, HazardActor, NPCActor, LoreItem, EquipmentItem } from "../foundry";
import { makeContiguousParser, ParserText } from "./parser";

export function sentenceCase(name: string) {
    return name.split(' ')
        .map(w => w.length > 1 ? w[0].toUpperCase() + w.substr(1).toLowerCase() : w)
        .join(' ')
        .trim();
}

export class LeftTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 76.5, 300, 711, 40, emit);
        this.getAllTextInBoundingBox(pdf, 315.05, 550, 711, 40, emit);
    }
};

export class RightTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 290, 711, 40, emit);
        this.getAllTextInBoundingBox(pdf, 306, 515, 711, 40, emit);
    }
};


const parseIcon = (p: ParserText) => {
    return p.consume().text;
};

const parseItalic = makeContiguousParser("<i>", "</i>");
const parseBold = makeContiguousParser("<b>", "</b>");
const parseHeading = makeContiguousParser("<h3>", "</h3>");

const headingFontIds = ['GoodOT-News:12', 'GoodOT:12'];
const normalFontIds = ['SabonLTStd-Roman:9', 'Sabon-Roman:9'];
const italicFontIds = ['SabonLTStd-Italic:9'];
const boldFontIds = ['TimesNewRomanPS-BoldMT:9', 'TimesNewRomanPSMT:9'];

const paragraphFontIds = headingFontIds
    .concat(normalFontIds)
    .concat(italicFontIds)
    .concat(boldFontIds);


const parseParagraph = (p: ParserText) => {
    let current = '';

    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (headingFontIds.includes(peekFont)) {
            current += parseHeading(p);
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<p>" + current + "</p>";
};

const parseBlockquote = (p: ParserText) => {
    let current = '';
    while (p.anyRemaining()) {
        if (p.peek().fontId == 'GoodOT:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9') {
            current += parseBold(p);
        } else {
            break;
        }
    }
    return "<blockquote>" + current + "</blockquote>";
};

const parseAside = (p: ParserText) => {
    return new Journal(p.consume().text, parseBlockquote(p));
};

const parseSection = (p: ParserText) => {
    let current = '';
    const startingFont = p.peek().fontId;
    let ret = [];

    let name = p.consume().text;
    if (p.peek().fontId == 'GoodOT-Bold:14') {
        name += "(" + p.consume().text + ")";
    }

    const headingFonts = ['Taroca:16', 'GoodOT-Bold:12'];
    while (headingFonts.includes(p.peek().fontId)) {
        name += p.consume().text;
    }

    while (p.anyRemaining()) {
        let cur = p.peek();
        if (cur.fontId == 'SabonLTStd-Roman:9' || p.peek().fontId == 'SabonLTStd-Italic:9' || p.peek().fontId == 'TimesNewRomanPS-BoldMT:9') {
            current += parseParagraph(p);
        } else if (cur.fontId == 'GoodOT:9' || p.peek().fontId == 'GoodOT-Bold:9') {
            current += parseBlockquote(p);
        } else if (cur.fontId == 'GoodOT-CondBold:12') {
            //current += parseMonster(p);
        } else if (cur.fontId == 'GoodOT-Bold:11') {
            ret.push(parseAside(p));
        } else if (cur.fontId == 'GoodOT-Bold:12' && !(cur.text.match(/^[ABCDEFG]\d+\./) || cur.text.match(/^Event \d+:/))) {
            current += parseHeading(p);
        } else {
            //console.log(`abandoning section on: ${cur.fontId} :: (${cur.x}, ${cur.y}) :: ${cur.text}`);
            break;
        }
    }

    ret.push(new Journal(name, current));
    return ret;
};

const parseHazardLine = (p: ParserText) => {
    let current = '';

    let isFirstElement = true;
    while (p.anyRemaining()) {
        if (!isFirstElement && p.peek().isLeftAligned && p.peek().fontId == 'GoodOT-Bold:9'
            && !['Critical Success', 'Success', 'Failure', 'Critical Failure'].includes(p.peek().text.trim()))
            break;
        isFirstElement = false;

        if (p.peek().fontId == 'GoodOT:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'Dax-Regular:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9' || p.peek().fontId == 'Dax-Bold:9') {
            current += parseBold(p);
        } else if (p.peek().fontId == 'Pathfinder-Icons:9') {
            current += parseIcon(p);
        } else {
            break;
        }
    }

    return current;
}

const parseAbility = (data: string) => {
    const value = parseInt(data, 10);
    return {
        value: value * 2 + 10,
        mod: value
    };
}

export const parseMonster = (p: ParserText, moduleData: ModuleData, name: string, level: number, tags: string[], qty: number) => {
    let npc = new NPCActor();
    
    //console.log('------'+name+'------');

    npc.name = name;
    npc.data.details.level.value = level;

    for (let trait of tags) {
        trait = trait.toLocaleLowerCase().trim();

        if (['LG', 'LN', 'LE', 'NG', 'N', 'NE', 'CG', 'CN', 'CE'].includes(trait.toLocaleUpperCase())) {
            npc.data.details.alignment.value = trait.toLocaleUpperCase();
        } else if (trait == 'tiny') {
            npc.data.traits.size.value = 'tiny';
        } else if (trait == 'small') {
            npc.data.traits.size.value = 'sm';
        } else if (trait == 'medium') {
            npc.data.traits.size.value = 'med';
        } else if (trait == 'large') {
            npc.data.traits.size.value = 'lg';
        } else if (trait == 'huge') {
            npc.data.traits.size.value = 'huge';
        } else if (trait == 'gargantuan') {
            npc.data.traits.size.value = 'grg';
        } else {
            npc.data.traits.traits.value.push(trait);
        }
    }

    while (p.anyRemaining()) {
        let next = p.peek();
        if (!['GoodOT:9', 'Dax-Regular:9', 'GoodOT-Italic:9', 'GoodOT-Bold:9'].includes(next.fontId))
            break;

        let line = parseHazardLine(p)
            .replace(/–/g, '-');

        let match, dmgMatch;
        if (match = line.match(/^<b>Perception<\/b> ([+-]?\d+)(; (.*))?$/)) {
            npc.data.attributes.perception.value = parseInt(match[1], 10);
            npc.data.traits.senses.value = match[3];
        } else if (match = line.match(/^<b>Languages<\/b> (.*)$/)) {
            npc.data.traits.languages.value = match[1].split(',').map(x => x.trim());
        } else if (match = line.match(/^<b>Skills<\/b> (.*)$/)) {
            const skills = match[1].split(',');
            for (const skill of skills) {
                const skillMatch = skill.trim().match(/(.*) ([+-]\d+)/);
                if (!skillMatch) continue;

                const lore = new LoreItem();
                lore.name = skillMatch[1];
                lore.data.mod.value = parseInt(skillMatch[2], 10);
                npc.items.push(lore);
            }
        } else if (match = line.match(/^<b>Skills<\/b> (.*)$/)) {
            const skills = match[1].split(',');
            for (const skill of skills) {
                const skillMatch = skill.trim().match(/(.*) ([+-]\d+)/);
                if (!skillMatch) continue;

                const lore = new LoreItem();
                lore.name = skillMatch[1];
                lore.data.mod.value = parseInt(skillMatch[2], 10);
                npc.items.push(lore);
            }
        } else if (match = line.match(/^<b>Items<\/b> (.*)$/)) {
            const items = match[1].split(',');
            for (const item of items) {
                const itemMatch = item.trim().match(/^(.*?)( \((\d+)\))?$/);
                if (!itemMatch) continue;

                const equipmentItem = new EquipmentItem();
                equipmentItem.name = itemMatch[1];
                equipmentItem.data.description.value = '';
                if (itemMatch[3]) {
                    equipmentItem.data.quantity.value = parseInt(itemMatch[3], 10);
                }
                npc.items.push(equipmentItem);
            }
        } else if (match = line.match(/^<b>Str<\/b> ([+-]?\d+), <b>Dex<\/b> ([+-]?\d+), <b>Con<\/b> ([+-]?\d+), <b>Int<\/b> ([+-]?\d+), <b>Wis<\/b> ([+-]?\d+), <b>Cha<\/b> ([+-]?\d+)$/)) {
            npc.data.abilities.str = parseAbility(match[1]);
            npc.data.abilities.dex = parseAbility(match[2]);
            npc.data.abilities.con = parseAbility(match[3]);
            npc.data.abilities.int = parseAbility(match[4]);
            npc.data.abilities.wis = parseAbility(match[5]);
            npc.data.abilities.cha = parseAbility(match[6]);
        } else if (match = line.match(/^<b>AC<\/b> ([+-]?\d+); <b>Fort<\/b> ([+-]?\d+), <b>Ref<\/b> ([+-]?\d+), <b>Will<\/b> ([+-]?\d+)/)) {
            npc.data.attributes.ac.value = parseInt(match[1], 10);
            npc.data.saves.fortitude.value = parseInt(match[2], 10);
            npc.data.saves.reflex.value = parseInt(match[3], 10);
            npc.data.saves.will.value = parseInt(match[4], 10);
        } else if (match = line.match(/^<b>HP<\/b> ([+-]?\d+)/)) {
            const hp = parseInt(match[1], 10);
            npc.data.attributes.hp.value = hp;
            npc.data.attributes.hp.max = hp;
        } else if (match = line.match(/^<b>Speed<\/b> (.+)$/)) {
            npc.data.attributes.speed.value = match[1];
        } else if (match = line.match(/^<b>(Melee|Ranged)<\/b> ?(\[one-action\])? ?(.*) (\+?\d+)( \((.*)\))?, (<b>Damage<\/b> ?(.*?))?(<b>Effect<\/b> (.*?))?(; no multiple attack penalty)?$/)) {
            let attack = new MeleeItem();
            if (match[1] == "Ranged") attack.data.weaponType.value = "ranged";
            attack.name = match[3];
            attack.data.bonus.value = parseInt(match[4], 10);

            if (match[8] && (dmgMatch = match[8].match(/^(.*?) (\w+)( plus (.*?))?$/))) {
                attack.data.damageRolls.push({ damage: dmgMatch[1], damageType: dmgMatch[2] });
                if (dmgMatch[4]) {
                    attack.data.attackEffects.value.push(dmgMatch[4]);
                }
            }
            
            if (match[10]) {
                attack.data.attackEffects.value.push(match[10].trim());
            }

            if (match[11]) {
                attack.data.attackEffects.value.push("no multiple attack penalty");
            }

            npc.items.push(attack);
        } else if (match = line.match(/<b>(.*?)<\/b> ?\[(reaction|free-action)\]( \((.*)\);? )?(<b>Trigger<\/b>.*<b>Effect ?<\/b>.*)/)) {
            let reaction = new ActionItem();
            if (match[2] == 'reaction') {
                reaction.data.actionType.value = 'reaction';
                reaction.img = 'systems/pf2e/icons/actions/Reaction.png';
            } else if (match[2] == 'free-action') {
                reaction.data.actionType.value = 'free';
                reaction.img = 'systems/pf2e/icons/actions/FreeAction.png';
            }
            reaction.name = match[1];
            reaction.data.description.value = match[5];
            if (match[4]) {
                let traits = match[4].split(',');
                for (let trait of traits)
                    reaction.data.traits.value.push(trait.trim());
            }
            npc.items.push(reaction);            
        } else if (match = line.match(/<b>(.*?)<\/b> ?\[(one-action|two-actions)\]( \((.*)\);? )?(.*)/)) {
            let action = new ActionItem();
            if (match[2] == 'one-action') {
                action.data.actionType.value = 'action';
                action.data.actions.value = '1';
                action.img = 'systems/pf2e/icons/actions/OneAction.png';
            } else if (match[2] == 'two-actions') {
                action.data.actionType.value = 'action';
                action.data.actions.value = '2';
                action.img = 'systems/pf2e/icons/actions/TwoActions.png';
            }
            action.name = match[1];
            action.data.description.value = match[5];
            if (match[4]) {
                let traits = match[4].split(',');
                for (let trait of traits)
                    action.data.traits.value.push(trait.trim());
            }
            npc.items.push(action);
        } else if (match = line.match(/<b>(.*?)<\/b>( \((.*?)\);? )?(.*)$/)) {
            //console.log(line);
            let passive = new ActionItem();
            passive.data.actionType.value = "passive";
            passive.img = 'systems/pf2e/icons/actions/Passive.png';
            passive.name = match[1].trim();
            passive.data.description.value = match[4].trim();
            if (match[3]) {
                let traits = match[3].split(',');
                for (let trait of traits)
                    passive.data.traits.value.push(trait.trim());
            }
            npc.items.push(passive);
        } else {
            console.log("unknown npc line (" + name + "): "+line);
        }
    }

    moduleData.actors.push(npc);

    if (qty !== undefined) {
        name += " (" + qty + ")";
    }
    return `<h4>${name} - Creature ${level}</h4>`;
};

export const parseHazard = (p: ParserText, moduleData: ModuleData, name: string, level: number, tags: string[], qty: number) => {
    let hazard = new HazardActor();

    hazard.name = name;
    hazard.data.details.level = level;

    for (let trait of tags) {
        trait = trait.toLocaleLowerCase();
        if (trait == 'trap')
            continue;
        if (trait == 'complex') {
            hazard.data.details.isComplex = true;
            continue;
        }            
        hazard.data.traits.traits.value.push(trait);
    }

    let current = '';
    while (p.anyRemaining()) {
        let next = p.peek();
        if (!['GoodOT:9', 'Dax-Regular:9', 'GoodOT-Italic:9', 'GoodOT-Bold:9'].includes(next.fontId))
            break;

        let line = parseHazardLine(p);

        line = line.replace('(arcane, conjuration, summon;', '(arcane, conjuration, summon);');

        let match, dmgMatch;
        if (match = line.match(/^<b>Stealth<\/b> ?DC (\d+)( (.*))?$/)) {
            hazard.data.attributes.stealth.value = parseInt(match[1], 10) - 10;
            hazard.data.attributes.stealth.details = match[3];
        } else if (match = line.match(/^<b>Stealth<\/b> ?(\+\d+) (.*)$/)) {
            hazard.data.attributes.stealth.value = parseInt(match[1], 10);
            hazard.data.attributes.stealth.details = match[2];
        } else if (match = line.match(/^<b>Description<\/b> ?(.*)$/)) {
            hazard.data.details.description = match[1];
        } else if (match = line.match(/^<b>Disable<\/b> ?(.*)$/)) {
            hazard.data.details.disable = match[1];
        } else if (match = line.match(/^<b>Routine<\/b> ?(.*)$/)) {
            hazard.data.details.routine = match[1];
        } else if (match = line.match(/^<b>Reset<\/b> ?(.*)$/)) {
            hazard.data.details.reset = match[1];
        } else if (match = line.match(/^<b>AC<\/b> ?(\d+); <b>Fort<\/b> ?(\+?\d+),? <b>Ref<\/b> ?(\+?\d+)(,? <b>Will<\/b> ?(\+?\d+))?$/)) {
            hazard.data.attributes.hasHealth = true;
            hazard.data.attributes.ac.value = parseInt(match[1], 10);
            hazard.data.saves.fortitude.value = parseInt(match[2], 10);
            hazard.data.saves.reflex.value = parseInt(match[3], 10);
            if (match[5]) {
                hazard.data.saves.will.value = parseInt(match[5], 10);
            }
        } else if (match = line.match(/^(<b>.*?Hardness<\/b> (\d+), )?<b>.*?HP ?<\/b> ?(\d+)( \(BT \d+\))?(.*?)(; <b>Immunities<\/b> ?(.*))?$/)) {
            hazard.data.attributes.hasHealth = true;
            hazard.data.attributes.hardness = parseInt(match[2]??'0', 10) ?? 0;
            hazard.data.attributes.hp.value = parseInt(match[3], 10);
            hazard.data.attributes.hp.max = parseInt(match[3], 10);
            if (match[5]) {
                hazard.data.attributes.hp.details = match[5].trim();
            }
            let immunities = (match[7] || '').split(',');
            for (let immunity of immunities) {
                hazard.data.traits.di.value.push(immunity.trim().replace(' ', '-'));
            }
        } else if (match = line.match(/^<b>(Melee|Ranged)<\/b>(\[one-action\])? (.*) (\+?\d+)( \((.*)\))?, (<b>Damage<\/b> ?(.*?))?(<b>Effect<\/b> (.*?))?(; no multiple attack penalty)?$/)) {
            let attack = new MeleeItem();
            if (match[1] == "Ranged") attack.data.weaponType.value = "ranged";
            attack.name = match[3];
            attack.data.bonus.value = parseInt(match[4], 10);

            if (match[8] && (dmgMatch = match[8].match(/^(.*?) (\w+)( plus (.*?))?$/))) {
                attack.data.damageRolls.push({ damage: dmgMatch[1], damageType: dmgMatch[2] });
                if (dmgMatch[4]) {
                    attack.data.attackEffects.value.push(dmgMatch[4]);
                }
            }
            
            if (match[10]) {
                attack.data.attackEffects.value.push(match[10].trim());
            }

            if (match[11]) {
                attack.data.attackEffects.value.push("no multiple attack penalty");
            }

            hazard.items.push(attack);
        } else if (match = line.match(/<b>(.*?)<\/b> ?\[(reaction|free-action)\]( \((.*)\);? )?(<b>Trigger<\/b>.*<b>Effect ?<\/b>.*)/)) {
            let reaction = new ActionItem();
            if (match[2] == 'reaction') {
                reaction.data.actionType.value = "reaction";
                reaction.img = 'systems/pf2e/icons/actions/Reaction.png';
            } else if (match[2] == 'free-action') {
                reaction.data.actionType.value = 'free';
                reaction.img = 'systems/pf2e/icons/actions/FreeAction.png';
            }
            reaction.name = match[1];
            reaction.data.description.value = match[5];
            if (match[4]) {
                let traits = match[4].split(',');
                for (let trait of traits)
                    reaction.data.traits.value.push(trait.trim());
            }
            hazard.items.push(reaction);
        } else if (match = line.match(/<b>(.*?)<\/b>( \((.*?)\);? )?(.*)$/)) {
            //console.log(line);
            let passive = new ActionItem();
            passive.data.actionType.value = "passive";
            passive.img = 'systems/pf2e/icons/actions/Passive.png';
            passive.name = match[1].trim();
            passive.data.description.value = match[4].trim();
            if (match[3]) {
                let traits = match[3].split(',');
                for (let trait of traits)
                    passive.data.traits.value.push(trait.trim());
            }
            hazard.items.push(passive);
        } else {
            console.log(name + ":: " + line);
        }
    }

    if (name != 'Hazard Name')
    {
        moduleData.actors.push(hazard);
    }

    if (qty !== undefined) {
        name += " (" + qty + ")";
    }
    return `<h4>${name} - Hazard ${level}</h4>`;
};

const parseMonsterReference = (p: ParserText, name: string, difficulty: string, qty: number) => {
    let current = '';
    let isFirst = true;
    while (p.anyRemaining()) {
        if (!isFirst && p.peek().isLeftAligned && p.peek().fontId == 'GoodOT-Bold:9') {
            current += "<br>";
        }

        if (p.peek().fontId == 'GoodOT:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'Dax-Regular:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9') {
            current += parseBold(p);
        } else if (p.peek().fontId == 'Pathfinder-Icons:9') {
            current += parseIcon(p);
        } else {
            break;
        }

        isFirst = false;
    }

    if (qty !== undefined) {
        name += " (" + qty + ")";
    }

    if (difficulty !== undefined && difficulty.length > 0) {
        name += " - " + difficulty;
    }

    return `<blockquote><h4>${name}</h4>${current}</blockquote>`;
};

export const parseMonsterOrHazard = (p: ParserText, moduleData: ModuleData) => {
    let name = p.consume().text;
    // there's an empty textblock after the name for some reason)
    while (p.peek().fontId == 'GoodOT-CondBold:12' || p.peek().fontId == 'Pathfinder-Icons:12')
        name += ' ' + p.consume().text;

    name = name.replace('–', '-')
        .replace(/ +/g, ' ')
        .trim();

    let difficulty = '';
    let match;
    if (match = name.match(/^(.*)((HAZARD|CREATURE|ITEM|FEAT) -?\d+)$/i)) {
        name = match[1];
        difficulty = match[2];
    }

    name = sentenceCase(name);

    const levelStr = difficulty
        .replace('HAZARD ', '')
        .replace('CREATURE ', '')
        .replace('ITEM ', '')
        .replace('FEAT ', '');
    let level = parseInt(levelStr, 10) ?? 0

    let qty;
    const qtyMatch = name.match(/(.*?) \((.*)\)/);
    if (qtyMatch) {
        name = qtyMatch[1];
        qty = qtyMatch[2];
    }

    let tags = [];
    while (p.anyRemaining() && p.peek().fontId == 'GoodOT-CondBold:7') {
        tags.push(p.consume().text);
    }

    if (tags.length == 0) {
        // sometimes there's "reference" hazards
        return parseMonsterReference(p, name, difficulty, qty);
    }

    if (difficulty.toUpperCase().startsWith("HAZARD")) {
        return parseHazard(p, moduleData, name, level, tags, qty);
    } else if (difficulty.toUpperCase().startsWith("CREATURE")) {
        return parseMonster(p, moduleData, name, level, tags, qty);
    } else {
        return parseMonsterReference(p, name, difficulty, undefined);
    }
};

export function extractDetails(text: Array<TextDetail>, moduleData: ModuleData): void {
    text = text.filter(t => t.fontId != 'RomicStd-Bold:12');

    let p = new ParserText(text);

    while (p.anyRemaining()) {
        let cur = p.peek();
        if (cur.fontId == 'GoodOT-Bold:14' || cur.fontId == 'RomicStd-Bold:14' || cur.fontId == 'RomicStd-Bold:32' || cur.fontId == 'RomicStd-Bold:33' || cur.fontId == 'GoodOT-Bold:11') {
            //console.log("starting section: " + cur.text);
            parseSection(p);
        // } else if (cur.fontId == 'SabonLTStd-Roman:9' || peek().fontId == 'SabonLTStd-Italic:9' || peek().fontId == 'TimesNewRomanPS-BoldMT:9') {
        //     const paragraph = parseParagraph();
        //     //document.getElementById('pageContainer').innerHTML += paragraph;
        // } else if (cur.fontId == 'GoodOT:9') {
        //     const blockquote = parseBlockquote();
        //     //document.getElementById('pageContainer').innerHTML += blockquote;
        // } else if (cur.fontId == 'GoodOT-Bold:11') {
        //     parseAside();
        } else if (cur.fontId == 'GoodOT-CondBold:12') {
            parseMonsterOrHazard(p, moduleData);
        } else {
            let item = p.consume();
            //console.log(`unknown ${item.fontId} :: ${item.page} (${item.x}, ${item.y}) :: ${item.text}`);
        }
    }
}

export const crbModule : ModuleDescription = {
    prefix: "crb",
    fullName: "Pathfinder 2E Core Rulebook",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'CORE RULEBOOK' && t.fontId == 'RomicStd-Bold:53.3514');
    },
    extractDetails: extractDetails,
    pages: [
        // hazards
        new LeftTwoColumn(523),
        new RightTwoColumn(524),
        new LeftTwoColumn(525),
        new RightTwoColumn(526),
        new LeftTwoColumn(527),
        new RightTwoColumn(528),
        new LeftTwoColumn(529),
        new RightTwoColumn(530),
    ],
    scenes: [
    ],
    journals: [
    ],
    finalize: (moduleData: ModuleData) => {
    }
};

export const gmgModule : ModuleDescription = {
    prefix: "gmg",
    fullName: "Pathfinder 2E Gamemastery Guide",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'GAMEMASTERY GUIDE' && t.fontId == 'RomicStd-Bold:53.3514');
    },
    extractDetails: extractDetails,
    pages: [
        // hazards
        new LeftTwoColumn(75),
        new RightTwoColumn(76),
        new LeftTwoColumn(77),
        new RightTwoColumn(78),
        new LeftTwoColumn(79),
        new RightTwoColumn(80),
        new LeftTwoColumn(81),
        new RightTwoColumn(82),
    ],
    scenes: [
    ],
    journals: [
    ],
    finalize: (moduleData: ModuleData) => {
    }
};
