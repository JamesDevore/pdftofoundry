import { ModuleDescription, ModuleData, AdventureImage, CaptionImage } from "./IAdventure";
import { ModuleScene, ModuleSceneTiled } from "./ModuleScene";
import { PdfData, PdfImage } from "../parse";
import * as PFS from "./pfs";

const adventureModule : ModuleDescription = {
    prefix: "pfs-1-12",
    fullName: "PFS #1-12 - The Burden of Envy",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'PATHFINDER SOCIETY SCENARIO #1–12' && t.fontId == 'GoodOT-CondBold:10' && t.page == 1);
    },
    extractDetails: PFS.extractDetails,
    pages: [
        new PFS.TwoColumnWithRightCutout(3, 365),
        new PFS.TwoColumn(4),
        new PFS.TwoColumnWithRightCutout(5, 570),
        new PFS.TwoColumn(6),
        new PFS.TwoColumn(7),
        new PFS.TwoColumn(8),
        new PFS.TwoColumn(9),
        new PFS.TwoColumn(10),
        new PFS.TwoColumn(11),
        new PFS.TwoColumn(12),
        new PFS.TwoColumn(13),
        new PFS.TwoColumn(14),
        new PFS.TwoColumn(15),
        new PFS.TwoColumn(16),
        new PFS.TwoColumn(17),
        new PFS.TwoColumn(18),
        new PFS.TwoColumn(19),
        new PFS.TwoColumn(20),

        // appendix
        new PFS.TwoColumnWithRightCutout(21, 560, 'appendix1'),
        new PFS.TwoColumnWithRightCutout(22, 500, 'appendix1'),
        
        new PFS.TwoColumnWithRightCutout(23, 540, 'appendix2a'),
        new PFS.TwoColumnWithRightCutout(24, 530, 'appendix2b'),
    ],

    scenes: [
        new ModuleScene({
            name: 'map1',
            nameInfo: { x: 102.2126, y: 684.7747 },
            nameFn: name => name.substring(3).trim(),
            page: 7, width: 1323, height: 867,
            imageLeft: 0, imageTop: 0, imageRight: 1000, imageBottom: 1000,
            imageRows: 10, imageCols: 10,
            rotateCW: 1,
            journals: [
                {"x":950,"y":650,"name":"A1."},
                {"x":550,"y":550,"name":"A2."},
                {"x":850,"y":1550,"name":"A3."},
                {"x":900,"y":800,"name":"A4."},
                {"x":750,"y":1050,"name":"A5."},
                {"x":950,"y":1350,"name":"A6."},
                {"x":550,"y":850,"name":"A7."},
                {"x":750,"y":1550,"name":"A8."}
            ],
        }),
        new ModuleScene({
            name: 'map2',
            nameInfo: { x: 72, y: 675 },
            nameFn: name => name.substring(3).trim(),
            page: 14, width: 1305, height: 1048,
            imageLeft: 10, imageTop: 11, imageRight: 1294, imageBottom: 1038,
            imageRows: 24, imageCols: 30,
        }),
        new ModuleScene({
            name: 'map3',
            nameInfo: { x: 72, y: 672.7747 },
            nameFn: name => name.substring(3).trim(),
            page: 16, width: 1049, height: 1305,
            imageLeft: 12, imageTop: 14, imageRight: 994, imageBottom: 1292,
            imageRows: 30, imageCols: 23,
        }),
        new ModuleScene({
            name: 'map4',
            nameInfo: { x: 72, y: 672.7747 },
            nameFn: name => name.substring(3).trim(),
            page: 17, width: 1301, height: 1045,
            imageLeft: 10, imageTop: 11, imageRight: 1290, imageBottom: 1035,
            imageRows: 30, imageCols: 24,
            rotateCW: 3
        }),
    ],
    journals: [
        new CaptionImage({ name: 'journal1', page: 1, width: 1217, height: 1308 }),
        
        new CaptionImage({ name: 'npc1', page: 4, width: 432, height: 413 }),
        new CaptionImage({ name: 'npc2', page: 25, width: 1182, height: 1182 }),
        new CaptionImage({ name: 'npc3', page: 26, width: 1182, height: 1182 }),
        new CaptionImage({ name: 'npc4', page: 27, width: 1182, height: 1182 }),
        new CaptionImage({ name: 'npc5', page: 28, width: 1182, height: 1182 }),
        new CaptionImage({ name: 'npc6', page: 29, width: 1259, height: 1259 }),
        
    ],
    finalize: (moduleData: ModuleData) => {
    }
};

export default adventureModule;