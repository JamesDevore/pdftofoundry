import { crbModule, gmgModule }  from './crb'
import fallofplaguestone from './fallofplaguestone'
import ageOfAshes1 from './ageofashes1'
import ageOfAshes2 from './ageofashes2'
import ageOfAshes3 from './ageofashes3'
import ageOfAshes4 from './ageofashes4'
import ageOfAshes5 from './ageofashes5'
import ageOfAshes6 from './ageofashes6'
import pfs1_01 from './pfs1.01'
import pfs1_05 from './pfs1.05'
import pfs1_06 from './pfs1.06'
import pfs1_12 from './pfs1.12'
import pfs1_16 from './pfs1.16'
import pfs1_17 from './pfs1.17'
import pfsq2 from './pfsq2'
import pfsq3 from './pfsq3'

export default [
    crbModule,
    gmgModule,
    fallofplaguestone,
    ageOfAshes1,
    ageOfAshes2,
    ageOfAshes3,
    ageOfAshes4,
    ageOfAshes5,
    ageOfAshes6,
    pfs1_01,
    pfs1_05,
    pfs1_06,
    pfs1_12,
    pfs1_16,
    pfs1_17,
    pfsq2,
    pfsq3
];