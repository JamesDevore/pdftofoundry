import { ModuleScene } from "./ModuleScene";
import { PdfData, TextDetail } from "../parse";
import { PageLayout } from "./PageLayouts";
import { Journal, IActor } from "../foundry";
import { IModuleOutput } from "src/IModuleOutput";

export class ModuleData {
    journalEntries: Journal[] = [];
    actors: IActor[] = [];
}

export interface ModuleJournal {
    name: string;
    page: number;
    width: number;
    height: number;
    import(parsedPdf: PdfData, output: IModuleOutput) : Promise<void>;
};

export interface ModuleDescription {
    prefix: string;
    fullName: string;
    pages: PageLayout[];
    scenes: ModuleScene[];
    journals: ModuleJournal[];
    detect(pdf: PdfData): boolean;
    extractDetails(text: Array<TextDetail>, moduleData: ModuleData) : void;
    finalize(moduleData: ModuleData) : void;
};

export class AdventureImage {
    name: string;
    page: number;
    width: number;
    height: number;

    constructor({name, page, width, height }: { name: string, page: number, width: number, height: number }) {
        this.name = name;
        this.page = page;
        this.width = width;
        this.height = height;
    }

    async import(parsedPdf: PdfData, output: IModuleOutput) {
        const pdfImage = parsedPdf.imageDetails.find(x => x.page == this.page && x.width == this.width && x.height == this.height);
        if (pdfImage !== undefined) {
            let blob = await pdfImage.getImage();
            const finalPath = await output.emitFile(`images`, `${this.name}.png`, blob);
            output.createJournal(new Journal(this.name, '', finalPath));
        }
    }
}

export class CaptionImage {
    name: string;
    page: number;
    width: number;
    height: number;

    constructor({name, page, width, height }: { name: string, page: number, width: number, height: number }) {
        this.name = name;
        this.page = page;
        this.width = width;
        this.height = height;
    }

    async import(parsedPdf: PdfData, output: IModuleOutput) {
        const pdfImage = parsedPdf.imageDetails.find(x => x.page == this.page && x.width == this.width && x.height == this.height);
        if (pdfImage === undefined) {
            console.log('failed finding image');
            return;
        }

        let blob = await pdfImage.getImage();
        const finalPath = await output.emitFile(`images`, `${this.name}.png`, blob);

        const captions = parsedPdf.textDetails.filter(x => ['CaslonAntique:12', 'Taroca:12', 'RomicStd-Bold:12'].includes(x.fontId) && x.page == this.page);
        //console.log(captions);

        let caption = this.name;
        if (captions.length > 0) {
            caption = captions[0].text;
        }

        output.createJournal(new Journal(caption, '', finalPath));
    }
}