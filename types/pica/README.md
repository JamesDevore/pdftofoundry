# Installation
> `npm install --save @types/pica`

# Summary
This package contains type definitions for pica (https://github.com/nodeca/pica).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/pica.

### Additional Details
 * Last updated: Fri, 27 Dec 2019 15:32:31 GMT
 * Dependencies: none
 * Global values: `Pica`

# Credits
These definitions were written by Hamit YILMAZ (https://github.com/hmtylmz).
